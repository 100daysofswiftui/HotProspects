//
//  SortableProspectView.swift
//  HotProspects
//
//  Created by Pascal Hintze on 17.03.2024.
//

import SwiftUI

struct SortableProspectsView: View {
    let filter: ProspectsView.FilterType
    @State private var sortOrder = SortDescriptor(\Prospect.name)
    
    var body: some View {
        NavigationStack {
            ProspectsView(filter: filter, sort: sortOrder)
                .toolbar {
                    ToolbarItem(placement: .topBarTrailing) {
                        Menu {
                            Picker("Sort Order", selection: $sortOrder) {
                                Text("Sort by Name").tag(SortDescriptor(\Prospect.name))
                                Text("Sort by Most Recent").tag(SortDescriptor(\Prospect.creationDate, order: .reverse))
                            }
                        } label: {
                            Label("Sort", systemImage: "line.3.horizontal.decrease.circle")
                        }
                    }
                }
        }
    }
}

#Preview {
    SortableProspectsView(filter: .none)
}
