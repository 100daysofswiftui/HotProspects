//
//  EditView.swift
//  HotProspects
//
//  Created by Pascal Hintze on 17.03.2024.
//

import SwiftData
import SwiftUI

struct EditView: View {
    @Bindable var prospect: Prospect

    var body: some View {
        Form {
            TextField("Name", text: $prospect.name)
                .textContentType(.name)

            TextField("Email address", text: $prospect.emailAddress)
                .textContentType(.emailAddress)

            Toggle("Contacted", isOn: $prospect.isContacted)
        }
        .navigationTitle("Edit Prospect")
        .navigationBarTitleDisplayMode(.inline)
    }
}

#Preview {
    do {
        let config = ModelConfiguration(isStoredInMemoryOnly: true)
        let container = try ModelContainer(for: Prospect.self, configurations: config)
        let example = Prospect(name: "Example User", emailAddress: "example@user.com", isContacted: false)

        return EditView(prospect: example)
            .modelContainer(container)
    } catch {
        return Text("Failed to create preview: \(error.localizedDescription)")
    }
}
