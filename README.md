# HotProspects

HotProspects is an app to track who you meet at conferences. It will show a QR code that stores your attendee information, then others can scan that code to add you to their list of possible leads for later follow up.

This app was developed as part of the [100 Days of SwiftUI](https://www.hackingwithswift.com/books/ios-swiftui/hot-prospects-introduction) course from Hacking with Swift.

## Topics
The following topics were handled in this project:
- List selection
- Tab views
- Result
- Image interpolation
- Context menus
- Swipe actions
- Local notifications
- Swift package dependencies
- Tab views
- SwiftData filtering
- QR code
- UserNotifications framework